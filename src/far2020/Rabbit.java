package far2020;

import java.util.List;

/**
 * A simple model of a rabbit. Rabbits age, move, breed, and die.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2016.02.29
 * 
 * Refactored by Anderson Powers, 1/27/2020
 */
public class Rabbit extends Animal {
	// The age at which a rabbit can start to breed.
	private static final int BREEDING_AGE = 5;
	// The age to which a rabbit can live.
	private static final int MAX_AGE = 40;
	// The likelihood of a rabbit breeding.
	private static final double BREEDING_PROBABILITY = 0.12;
	// The maximum number of births.
	private static final int MAX_LITTER_SIZE = 4;

	/**
	 * Create a new rabbit. A rabbit may be created with age zero (a new born)
	 * or with a random age.
	 * 
	 * @param randomAge
	 *            If true, the rabbit will have a random age.
	 * @param field
	 *            The field currently occupied.
	 * @param location
	 *            The location within the field.
	 */
	public Rabbit(boolean randomAge, Field field, Location location) {
		super(randomAge, field, location);
	}

	/**
	 * @return the breedingAge
	 */
	@Override
	public int getBreedingAge() {
		return BREEDING_AGE;
	}

	/**
	 * @return the maxAge
	 */
	@Override
	public int getMaxAge() {
		return MAX_AGE;
	}

	/**
	 * @return the breedingProbability
	 */
	@Override
	public double getBreedingProbability() {
		return BREEDING_PROBABILITY;
	}

	/**
	 * @return the maxLitterSize
	 */
	@Override
	public int getMaxLitterSize() {
		return MAX_LITTER_SIZE;
	}

	/**
	 * This is what the rabbit does most of the time - it runs around. Sometimes
	 * it will breed or die of old age.
	 * 
	 * @param newActors
	 *            A list to return newly born rabbits.
	 */
	public void act(List<Actor> newActors) {
		incrementAge();
		if (isActive()) {
			giveBirth(newActors);
			// Try to move into a free location.
			Location newLocation = field.freeAdjacentLocation(getLocation());
			if (newLocation != null) {
				setLocation(newLocation);
			} else {
				// Overcrowding.
				setInactive();
			}
		}
	}

	@Override
	public Animal makeNewAnimal(boolean randomAge, Field field, Location location) {
		return new Rabbit(randomAge, field, location);
	}
}
