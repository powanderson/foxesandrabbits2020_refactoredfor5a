package far2020;

import java.util.List;
import java.util.Random;

/**
 * A class containing the characteristics of animals. Animals can breed, act,
 * age, and die.
 * 
 * @author Anderson Powers, 1/27/2020
 */

public abstract class Animal extends Actor {

	/**
	 * @return the maximum number of offspring the animal can have.
	 */
	public abstract int getMaxLitterSize();

	/**
	 * @return the probability that this animal will breed.
	 */
	public abstract double getBreedingProbability();

	/**
	 * @return the maximum age this animal can be before it dies.
	 */
	public abstract int getMaxAge();

	/**
	 * @return the minimum age this animal must be before it can breed.
	 */
	public abstract int getBreedingAge();

	/**
	 * This method should be overridden with just one line like this: return new
	 * Animal(randomAge, field, location); where Animal is replaced with whatever
	 * the type of the subclass is.
	 * 
	 * @param randomAge Should the age be determined randomly? Else it will be set
	 *                  to 0.
	 * @param field     The field to place the animal on.
	 * @param location  The location on the field to place the animal.
	 * @return A new animal of the given type
	 */
	public abstract Animal makeNewAnimal(boolean randomAge, Field field, Location location);

	// ----------------------------------------------------------------------------------
	protected static final Random rand = Randomizer.getRandom();

	private int age;

	public Animal(boolean randomAge, Field field, Location location) {
		super(field, location);
		age = 0;
		this.field = field;
		if (randomAge) {
			age = rand.nextInt(getMaxAge());
		}
	}

	/**
	 * Increase the age. This could result in the animal's death.
	 */
	protected void incrementAge() {
		age++;
		if (age > getMaxAge()) {
			setInactive();
		}
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	protected int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= getBreedingProbability()) {
			births = rand.nextInt(getMaxLitterSize()) + 1;
		}
		return births;
	}

	/**
	 * An animal can breed if it has reached the breeding age.
	 * 
	 * @return true if the animal can breed, false otherwise.
	 */
	protected boolean canBreed() {
		return age >= getBreedingAge();
	}

	/**
	 * Check whether or not this animal is to give birth at this step. New births
	 * will be made into free adjacent locations.
	 * 
	 * @param newActors A list to return newly born actors.
	 */
	protected void giveBirth(List<Actor> newActors) {
		// New animals are born into adjacent locations.
		// Get a list of adjacent free locations.
		List<Location> free = field.getFreeAdjacentLocations(location);
		int births = breed();
		for (int b = 0; b < births && free.size() > 0; b++) {
			Location loc = free.remove(0);
			Animal animal = makeNewAnimal(false, field, loc);
			newActors.add(animal);
		}
	}

}