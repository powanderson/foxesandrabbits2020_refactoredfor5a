package far2020;

import java.util.Random;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
// Added the following lines
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simple predator-prey simulator, based on a rectangular field containing
 * actors.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2016.02.29
 * 
 *          Refactored by Charles Cusack, 1/23/2020
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 120;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 80;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.02;
	// The probability that a rabbit will be created in any given position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;

	// Lists of animals in the field.
	private List<Actor> actors;
	// The current state of the field.
	private Field field;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (add your buttons, etc.).
	JButton runOneButton;
	JButton runHundredButton;
	JButton runLongButton;
	JButton runStepsButton;
	JButton resetButton;
	JMenuBar fileMenuBar;
	JMenuBar helpMenuBar;
	private JFrame mainFrame;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth Depth of the field. Must be greater than zero.
	 * @param width Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be >= zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}

		actors = new ArrayList<>();
		field = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.ORANGE);
		view.setColor(Fox.class, Color.BLUE);
		view.setColor(Virus.class, Color.GREEN);
		view.setColor(Mine.class, Color.RED);

		mainFrame = new JFrame();
		mainFrame.setTitle("Survival Simulation");

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Components:
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(1);
			}
		});

		runHundredButton = new JButton("Run 100 steps");
		runHundredButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(100);
			}
		});

		runLongButton = new JButton("Run 4000 steps");
		runLongButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLongSimulation();
			}
		});

		JTextField textfield = new JTextField(5);
		textfield.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = textfield.getText();
				textfield.selectAll();
			}
		});

		runStepsButton = new JButton("Run # of steps:");
		runStepsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = textfield.getText();
				try {
					int steps = Integer.parseInt(text);
					simulate(steps);
				} catch (NumberFormatException exception) {
					System.out.println("error");
				}
			}
		});

		resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});

		fileMenuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitItem = new JMenuItem("Exit");
		fileMenuBar.add(fileMenu);
		fileMenu.add(exitItem);
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		helpMenuBar = new JMenuBar();
		JMenu helpMenu = new JMenu("Help");
		JMenuItem aboutItem = new JMenuItem("About");
		fileMenuBar.add(helpMenu);
		helpMenu.add(aboutItem);
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(mainFrame,
						"A program to simulate the interaction " + "of different creatures. These include the fox"
								+ ", the rabbit, and the virus." + "\n\nWritten by Anderson Powers"
								+ "\n- January 30, 2020",
						"About", JOptionPane.PLAIN_MESSAGE);
			}
		});

		// Layout:
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER);

		JPanel buttons = new JPanel(layout);
		buttons.add(fileMenuBar);
		buttons.add(helpMenuBar);
		buttons.add(runOneButton);
		buttons.add(runHundredButton);
		buttons.add(runLongButton);
		buttons.add(runStepsButton);
		buttons.add(textfield);
		buttons.add(Box.createHorizontalStrut(5));
		buttons.add(resetButton);

		JPanel simulationView = new JPanel(new BorderLayout());
		simulationView.add(view);

		mainFrame.add(buttons, BorderLayout.NORTH);
		mainFrame.add(simulationView);

		mainFrame.pack();
		reset();
		mainFrame.setVisible(true);
	}

	/**
	 * Enables or disables multiple buttons.
	 * 
	 * @param enabled True if enabling buttons, false if disabling buttons.
	 */
	private void enableButtons(boolean enabled) {
		if (enabled) {
			runOneButton.setEnabled(true);
			runHundredButton.setEnabled(true);
			runLongButton.setEnabled(true);
			runStepsButton.setEnabled(true);
			resetButton.setEnabled(true);
		} else {
			runOneButton.setEnabled(false);
			runHundredButton.setEnabled(false);
			runLongButton.setEnabled(false);
			runStepsButton.setEnabled(false);
			resetButton.setEnabled(false);
		}
	}

	/**
	 * Run the simulation from its current state for a reasonably long period (4000
	 * steps).
	 */
	public void runLongSimulation() {
		simulate(4000);
	}

	/**
	 * Run the simulation for the given number of steps. Stop before the given
	 * number of steps if it ceases to be viable.
	 * 
	 * @param numSteps The number of steps to run for.
	 */
	public void simulate(int numSteps) {
		// For technical reason, I had to add numberSteps as a class variable.
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				enableButtons(false);
				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					simulateOneStep();
				}
				// Now re-enable the button
				enableButtons(true);
			}
		};
		// Start the thread
		runThread.start();
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over the
	 * whole field updating the state of each actor.
	 */
	public void simulateOneStep() {
		step++;

		// Provide space for new actors.
		List<Actor> newActors = new ArrayList<>();
		// Let all actors act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			actor.act(newActors);
			if (!actor.isActive()) {
				it.remove();
			}
		}

		// A small chance for a virus to be created
		if (Math.random() < .02) {
			Location randomLocation = new Location((int) (Math.random() * 80), (int) (Math.random() * 80));
			Virus virus = new Virus(field, randomLocation);
			actors.add(virus);
		}

		// A small chance for a mine to be created
		if (Math.random() < .5) {
			Location randomLocation = new Location((int) (Math.random() * 80), (int) (Math.random() * 80));
			Mine mine = new Mine(field, randomLocation);
			actors.add(mine);
		}

		// Add the new actors to the main lists.
		actors.addAll(newActors);

		view.showStatus(step, field);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		actors.clear();
		populate();

		// Show the starting state in the view.
		view.showStatus(step, field);
	}

	/**
	 * Randomly populate the field with actors.
	 */
	private void populate() {
		Random rand = Randomizer.getRandom();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Location location = new Location(row, col);
					Fox fox = new Fox(true, field, location);
					actors.add(fox);
				} else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Location location = new Location(row, col);
					Rabbit rabbit = new Rabbit(true, field, location);
					actors.add(rabbit);
				}
				// else leave the location empty.
			}
		}
		Location randomLocation = new Location(rand.nextInt(50), rand.nextInt(50));
		Virus virus = new Virus(field, randomLocation);
		actors.add(virus);
	}

	/**
	 * Pause for a given time.
	 * 
	 * @param millisec The time to pause for, in milliseconds
	 */
	private void delay(int millisec) {
		try {
			Thread.sleep(millisec);
		} catch (InterruptedException ie) {
			// wake up
		}
	}
}
