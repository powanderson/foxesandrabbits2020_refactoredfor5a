package far2020;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A mine that detonates, exploding adjacent animals, after 10 turns.
 * @author Anderson
 *
 */
public class Mine extends Actor {

	int countdown;

	/**
	 * @param field    The field to place the mine on.
	 * @param location The location within the field.
	 */
	public Mine(Field field, Location location) {
		super(field, location);
		countdown = 10;
	}

	/**
	 * Counts down each call until it reaches 0, when it detonates.
	 */
	public void act(List<Actor> newActors) {
		countdown--;
		if (countdown <= 0) {
			detonate();
			setInactive();
		}
	}

	/**
	 * Removes the animals adjacent to it.
	 */
	public void detonate() {
		List<Location> adjacentLocations = field.adjacentLocations(location);
		Iterator<Location> it = adjacentLocations.iterator();

		//Iterate through the adjacent locations
		while (it.hasNext()) {
			Location where = it.next();
			if (field.getObjectAt(where) instanceof Animal) {
				((Actor) field.getObjectAt(where)).setInactive();
				it.remove();
			}
			List<Location> adjacentLocs = field.adjacentLocations(where);
			List<Location> locsToRemove = new ArrayList<>();
			
			//Iterate through the adjacent locations of the adjacent locations.
			for (int i = 0; i < adjacentLocs.size(); i++) {
				if (field.getObjectAt(adjacentLocs.get(i)) instanceof Animal) {
					locsToRemove.add(adjacentLocs.get(i));
					((Actor) field.getObjectAt(adjacentLocs.get(i))).setInactive();
					adjacentLocs.remove(i);
				}
			}

		}
	}
}
