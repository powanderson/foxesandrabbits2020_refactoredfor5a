package far2020;

import java.util.List;

/**
 * Shared characteristics of a creature that can be active, inactive, has a
 * location, and has an action.
 * 
 * @author Anderson
 *
 */
public abstract class Actor {
	protected boolean active;
	protected Location location;
	protected Field field;

	/**
	 * Do whatever the actor is supposed to do each turn.
	 * @param newActors
	 */
	public abstract void act(List<Actor> newActors);

	public Actor(Field field, Location location) {
		active = true;
		this.field = field;
		setLocation(location);
	}

	/**
	 * Place the actor at the new location in the given field.
	 * 
	 * @param newLocation The actor's new location.
	 */
	protected void setLocation(Location newLocation) {
		if (location != null) {
			field.clear(location);
		}
		location = newLocation;
		field.place(this, newLocation);
	}

	/**
	 * Return the actor's location.
	 * 
	 * @return The actor's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Check whether the actor is alive or not.
	 * 
	 * @return true if the actor is still alive.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Indicate that the actor is no longer alive. It is removed from the field.
	 */
	public void setInactive() {
		active = false;
		if (location != null) {
			field.clear(location);
			location = null;
			field = null;
		}
	}

}
