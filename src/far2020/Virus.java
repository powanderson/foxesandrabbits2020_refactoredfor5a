package far2020;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Viruses that are spread by removing an adjacent actor and replacing it with a
 * new virus.
 * 
 * @author Anderson Powers, 1/27/2020
 *
 */
public class Virus extends Actor {

	int timeWithoutFood;
	Random random = new Random();

	/**
	 * How many viruses a single virus can create. (Not optimized to go over 2.)
	 */
	private static final int VIRUS_MULTIPLIER = 1;

	/**
	 * The number of turns a virus can go without "food"
	 */
	private static final int TIME_BEFORE_INACTIVITY = 8;

	/**
	 * @param field    The field to place the virus on.
	 * @param location The location within the field.
	 */
	public Virus(Field field, Location location) {
		super(field, location);
	}

	/**
	 * Creates new viruses adjacent to the existing virus. Moves the virus to an
	 * adjacent location.
	 */
	public void act(List<Actor> newActors) {
		if (isActive()) {
			List<Location> adjacentLocations = field.adjacentLocations(location);

			// Iterate through the newActors list
			ArrayList<Virus> virusesToAdd = new ArrayList<>();
			for (Iterator<Actor> it = newActors.iterator(); it.hasNext();) {
				Actor actor = it.next();

				// Ends the process of creating viruses if it has already created another virus
				if (virusesToAdd.size() >= VIRUS_MULTIPLIER) {
					break;
				}

				// Iterate through the adjacent locations
				for (int i = 0; i < adjacentLocations.size(); i++) {
					if (actor.getLocation() != null && !(actor instanceof Virus)) {

						// If an actor is on an adjacent location, mark it to be added
						if (actor.getLocation().equals(adjacentLocations.get(i))) {
							Virus newVirus = new Virus(field, location);
							newVirus.setLocation(adjacentLocations.get(i));
							virusesToAdd.add(newVirus);
							break;
						}
					}
				}
			}
			// Add the new viruses to the newActors list
			for (int i = 0; i < virusesToAdd.size(); i++) {
				newActors.add(virusesToAdd.get(i));
			}

			// Move the virus
			Location freeLocation = field.freeAdjacentLocation(location);
			if (freeLocation != null) {
				setLocation(freeLocation);
			}

			// If nothing was spread, add to the time without food
			if (virusesToAdd.size() == 0) {
				incrementTimeWithoutFood();
			}

		}
	}

	/**
	 * Increments the timeWithoutFood and sets the virus to inactive if it is over
	 * 7.
	 */
	public void incrementTimeWithoutFood() {
		timeWithoutFood++;
		if (timeWithoutFood > TIME_BEFORE_INACTIVITY) {
			setInactive();
		}
	}
}
